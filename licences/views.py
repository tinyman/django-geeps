from django.shortcuts import render, redirect
from .forms import LicenceForm
from .utils import send_mail
from .utils import create_pdf
# Create your views here.


def index(request):
    form = LicenceForm()
    return render(request, 'index.html', {'form': form})


def confirm(request):
    if request.method == "POST":
        form = LicenceForm(request.POST)
        if form.is_valid():
            send_mail(form)
    else:
        return redirect('index')
    return render(request, 'confirm.html')


def help(request, os='windows'):
    return render(request, 'help/%s.html' % os, {})
