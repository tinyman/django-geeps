import csv
import datetime
import io
import reportlab
from .forms import LicenceForm
from django.core.mail import EmailMessage
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, Image, Spacer
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm


def date():
    return f"{datetime.datetime.now():%Y-%m-%d}"


def time():
    return f"{datetime.datetime.now():%H:%M}"


def filename(form: LicenceForm, t="csv"):
    prefix = "matlab-%(release)s-%(firstname)s_%(name)s" % form.cleaned_data
    return f"{prefix}-{date()}.{t}"


def filenamePDF(form: LicenceForm, t="pdf"):
    prefix = "matlab-%(release)s-%(firstname)s_%(name)s" % form.cleaned_data
    return f"{prefix}-{date()}.{t}"


def create_csv(form: LicenceForm):
    content = io.StringIO()
    writer = csv.writer(content, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(["ActivationLabel", "Matlab Release", "HostID", "Name", "Firstname",
                     "Email", "Office", "Phone Number", "Building", "Operating System"])
    writer.writerow(["%(firstname)s-%(name)s-%(release)s-%(date)s" % {**form.cleaned_data, "date": date()}, form.cleaned_data.get('release'),
                     form.cleaned_data.get('hostid'), form.cleaned_data.get('name'), form.cleaned_data.get('firstname'), form.cleaned_data.get('email'), form.cleaned_data.get('bureau'), form.cleaned_data.get('telephone'), form.cleaned_data.get('batiment'), form.cleaned_data.get('os')])
    return content


def create_pdf(form: LicenceForm):
    buff = io.BytesIO()
    doc = SimpleDocTemplate(buff, pagesize=A4,
                            title='Licence Matlab activation request',
                            author='Django Matlab Licence request app',
                            topMargin=0)

    styles = getSampleStyleSheet()
    styleT = styles['Title']
    story = []
    story.append(Image('licences/static/images/Geeps-logo-gris-gras.jpg',
                       height=4*cm, width=4*cm, hAlign="LEFT"))
    story.append(Spacer(0*cm, 1*cm))
    story.append(Paragraph(_("Request summary (%(date)s at %(heure)s)") % {
                 "date": date(), "heure": time()}, styleT))
    story.append(Spacer(0*cm, 1*cm))
    data = [[_("Name"), form.cleaned_data.get("name")],
            [_("Firstname"), form.cleaned_data.get("firstname")],
            [_("Email"), form.cleaned_data.get("email")],
            [_("Office"), form.cleaned_data.get("bureau")],
            [_("Phone number"), form.cleaned_data.get("telephone")],
            [_("Building"), form.cleaned_data.get("batiment")],
            [_("Matlad Release"), form.cleaned_data.get("release")],
            [_("Operating System"), form.cleaned_data.get("os")],
            [_("HostID"), form.cleaned_data.get("hostid")], ]
    t = Table(data)
    story.append(t)
    doc.build(story)
    return buff


def send_mail(form: LicenceForm):
    email = EmailMessage(
        "Activation licence Matlab",
        "Demande d'activation de Matlab %(release)s de %(firstname)s %(name)s pour le systeme %(os)s." % form.cleaned_data,
        from_email=settings.SENDER_EMAIL,
        to=[settings.RECIPIENT_EMAIL],
    )
    csv = create_csv(form)
    pdf = create_pdf(form)
    email.attach(filename=filename(form),
                 content=csv.getvalue(), mimetype="text/csv"),
    email.attach(filename=filenamePDF(form),
                 content=pdf.getvalue(), mimetype="application/pdf")
    email.send(
        fail_silently=False
    )

    csv.close()

    confirmation_email = EmailMessage(
        _("Licence MatLab Activation Request"),
        _("Your MatLab release %(release)s request activation for your %(os)s operating system was well taken into account\n\nLe CRI du GeePs") % form.cleaned_data,
        from_email=settings.SENDER_EMAIL,
        to=[form.cleaned_data.get('email')],
    )
    confirmation_email.attach(filename=filenamePDF(form),
                              mimetype="application/pdf",
                              content=pdf.getvalue())
    confirmation_email.send(fail_silently=False)
    pdf.close()
