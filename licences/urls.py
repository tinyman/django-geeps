from django.urls import path, include

from . import views

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('', views.index, name='index'),
    path('confirm', views.confirm, name='confirm'),
    path('help/', views.help, name='help'),
    path('help/<os>', views.help, name='help'),
]
