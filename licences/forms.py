from django import forms
from django.utils.translation import ugettext_lazy as _


class LicenceForm(forms.Form):
    name = forms.CharField(label=_('Name'))
    firstname = forms.CharField(label=_('Firstname'))
    email = forms.EmailField(label=_('Email'))
    telephone = forms.CharField(label=_('Phone number'))
    bureau = forms.CharField(label=_('Office'))
    batiment = forms.ChoiceField(label=_('Building'), choices=(
        ('geeps', 'GeePs'),
        ('breguet', 'Breguet')))
    release = forms.ChoiceField(label=_('Matlab Release'), choices=(
        ('R2017b', 'R2017b'),
        ('R2016b', 'R2016b'),
        ('R2015b', 'R2015b'),
        ('R2014b', 'R2014b'),
        ('R2013b', 'R2013b')
    ))
    os = forms.ChoiceField(label=_('Operating System'), choices=(
        ('windows', 'Windows'),
        ('linux', 'Linux'),
        ('mac', 'MacOS')
    ))
    hostid = forms.CharField(label=_('HostID'))
