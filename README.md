# Installation
In your `virtualenv`:
```
git clone https://gitlab.com/tinyman/django-geeps.git
cd django-geeps
pip install -r requirements.txt
django-admin compilemessages
```

# Setup

Mail Settings in `projet/settings.py`:
```python
EMAIL_HOST = 'smtp.mailtrap.io' # SMTP Settings
EMAIL_HOST_USER = 'c0a06aab5b0174'
EMAIL_HOST_PASSWORD = '000d5bc631dcdb'
EMAIL_PORT = '2525'

SENDER_EMAIL = 'noreply@geeps.fr' # app email
RECIPIENT_EMAIL = 'cri@geeps.centralesupelec.fr' # admin email
```

# Features
* Licence request form
* Form validation
* Email confirmation with request summary (pdf)
* Email to admin with request details (pdf and csv)
* Translations (English and French)